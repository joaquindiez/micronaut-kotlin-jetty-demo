package com.example.controller

import io.micronaut.http.HttpResponse
import io.micronaut.http.annotation.Controller
import io.micronaut.http.annotation.Get


@Controller
class HelloWorldController {

    @Get("/hello")
    fun hello(): HttpResponse<String>{

        return HttpResponse.ok("hello World")
    }
}